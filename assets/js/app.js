$(function(){
    $('.datepicker').datetimepicker();

    $(".fancybox").fancybox({
        openEffect	: 'none',
        closeEffect	: 'none'
    });

    var script = document.createElement('script');
    document.body.appendChild(script);

    function mapInit() {
        var map;
        $('.mapBox').each(function() {
            var self = $(this);
            var myLatLng = {lat: self.data('lat'), lng: self.data('lng')};

            map = new google.maps.Map(this, {
                center: myLatLng,
                zoom: 14,
                scrollwheel: false
            });
            new google.maps.Marker({
                position: myLatLng,
                map: map
            });
        });
    }


    $('.galleryWidget li a').fancybox();
    
    var $isLoadedFaq = false;


    $('.lg-map').on('show.bs.modal', function () {
        resizeMap();
    });

    function resizeMap() {
        if (typeof map == "undefined")
            return;
        setTimeout(function () {
            resizingMap();
        }, 400);
    }

    function resizingMap() {
        if (typeof map == "undefined")
            return;
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    }
    
    var url = $('#packageUrl').val();
    
    
    var trackingMeta = {};
    $('.btn-social-share').click(function(){
        analytics.track('share-social',{
            listing_name: $(this).data('listing'),
            partner_name: $(this).data('partner'),
            chanel: $(this).data('type'),
            platform: app.platforms.web_app
        });
    });
});
